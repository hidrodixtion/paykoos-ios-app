//
//  ProfileVC.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ProfileVC: UIViewController {

    @IBOutlet weak var txtKosname: UILabel!
    @IBOutlet weak var txtKosPrice: UILabel!
    @IBOutlet weak var txtKosAddress: UILabel!
    @IBOutlet weak var txtKosPhone: UILabel!
    @IBOutlet weak var imgKos: UIImageView!
    @IBOutlet weak var txtKosTipe: UIView!
    @IBOutlet weak var viewKos: UIView!
    
    @IBOutlet weak var txtUsername: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtAddress: UILabel!
    @IBOutlet weak var txtPhone: UILabel!
    
    var data: JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "PayKoos"
        
        getData()
    }
    
    func getData() {
        Alamofire.request("http://paykoos.dev/profile.json").validate().responseJSON { [unowned self] response in
            switch response.result {
            case .success(let value):
                self.fillData(JSON(rawValue: value)!)
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func fillData(_ data: JSON) {
        txtUsername.text = data["username"].stringValue
        txtName.text = data["nama"].stringValue
        txtAddress.text = data["alamat"].stringValue
        txtPhone.text = data["telepon"].stringValue
        
        let kos = data["kos"]
        txtKosname.text = kos["nama"].stringValue
        txtKosAddress.text = kos["alamat"].stringValue
        txtKosPhone.text = kos["telepon"].stringValue
        txtKosPrice.text = PriceFormatter.format(numPrice: kos["harga"].numberValue)
        
        imgKos.af_setImage(withURL: URL(string: kos["image"].stringValue)!)
    }
}
