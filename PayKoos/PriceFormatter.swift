//
//  Formatter.swift
//  TantrazBooks
//
//  Created by Adi Nugroho on 1/10/17.
//  Copyright © 2017 Project Box. All rights reserved.
//

import Foundation

class PriceFormatter {
    private static let formatter = NumberFormatter()
    
    static func format(price: String) -> String {
        formatter.numberStyle = .currency
        formatter.currencySymbol = "Rp."
        formatter.maximumFractionDigits = 0
        formatter.currencyGroupingSeparator = "."
        
        let nprice = Float(price)! as NSNumber
        return formatter.string(from: nprice)!
    }
    
    static func format(numPrice: NSNumber) -> String {
        formatter.numberStyle = .currency
        formatter.currencySymbol = "Rp."
        formatter.maximumFractionDigits = 0
        formatter.currencyGroupingSeparator = "."
        
        return formatter.string(from: numPrice)!
    }
}
