//
//  DateHelper.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import SwiftMoment

class DateHelper {
    static let dformatter = DateFormatter()
    
    static func parseRange(startDate: String, endDate: String) -> String {
        let momStart = parseMoment(dateStr: startDate)
        let momEnd = parseMoment(dateStr: endDate)
        
        return "\(momStart.format("dd")) - \(momEnd.format("dd MMMM yyyy"))"
    }
    
    static func parseMoment(dateStr: String) -> Moment {
        return moment(dateStr, dateFormat: "yyyy/MM/dd", timeZone: .current, locale: Locale.init(identifier: "id"))!
    }
    
    static func parse(date: String) -> String {
        let mom = parseMoment(dateStr: date)
        
        return mom.format("dd MMMM yyyy")
    }
}
