//
//  VCTransaksi.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VCTransaksi: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var trans = [JSON]()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "PayKoos"
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 186.0
        tableView.dataSource = self
        tableView.delegate = self
        
        getDataTransaksi()
        
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        super.viewWillAppear(animated)
    }
    
    /**
     Retrieve transaction data
    */
    func getDataTransaksi() {
        Alamofire.request("http://paykoos.dev/transaksi.json")
            .validate()
            .responseJSON { [unowned self] response in
                switch response.result {
                case .success(let value):
                    self.trans = JSON(rawValue: value)!.arrayValue
                    self.tableView.reloadData()
                case .failure(let error):
                    print(error.localizedDescription)
                }
        }
    }
}

extension VCTransaksi: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentTrans = trans[indexPath.row]
        let isPaid = currentTrans["isPaid"].boolValue
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPaid", for: indexPath) as! CellTransaksi
        if isPaid {
            cell.type = .paid
            cell.viewTitle.backgroundColor = UIColor(named: .bluePayk)
            cell.txtInfoPaidDate.text = "Pembayaran diterima"
        } else {
            cell.type = .unpaid
            cell.viewTitle.backgroundColor = UIColor(named: .redPayk)
            cell.txtInfoPaidDate.text = "Batas pembayaran"
        }
        
        cell.txtTitle.textColor = UIColor.white
        cell.txtIDTransaksi.textColor = UIColor.white
        
        cell.txtTitle.text = currentTrans["kos"].stringValue
        cell.txtIDTransaksi.text = currentTrans["id"].stringValue
        cell.txtCost.text = PriceFormatter.format(numPrice: currentTrans["biaya"].numberValue)
        cell.txtDateRange.text = DateHelper.parseRange(startDate: currentTrans["start_date"].stringValue, endDate: currentTrans["end_date"].stringValue)
        
        return cell
    }
}

extension VCTransaksi: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected \(indexPath.row)")
        
        let cell = tableView.cellForRow(at: indexPath) as! CellTransaksi
        cell.contentView.backgroundColor = UIColor.clear
        
        let vcDetailTrans = StoryboardScene.Transaksi.instantiateVcDetilTransaksi()
        vcDetailTrans.transType = cell.type
        vcDetailTrans.data = trans[indexPath.row]
        navigationController?.pushViewController(vcDetailTrans, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.contentView.backgroundColor = UIColor.clear
    }
}
