//
//  ViewController.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/3/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    @IBAction func onLoginClicked(_ sender: UIButton) {
        present(StoryboardScene.MainPage.instantiateMainPageVC(), animated: true, completion: nil)
    }
}

