//
//  KosVC.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireImage

class KosVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var data = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "PayKoos"
        
        tableView.estimatedRowHeight = 210
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.dataSource = self
        tableView.delegate = self
        
        getData()
        
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    func getData() {
        Alamofire.request("http://paykoos.dev/kos.json").validate().responseJSON { [unowned self] response in
            switch response.result {
            case .success(let value):
                self.data = JSON(rawValue: value)!.arrayValue
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
}

extension KosVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellKos", for: indexPath) as! CellKos
        
        let currentData = data[indexPath.row]
//        let kamars = currentData["kamar"].arrayValue.sorted { data1, data2 in
//            data1["biaya"].intValue < data2["biaya"].intValue
//        }
//        let images = currentData["images"].arrayValue
        
        cell.txtKosName.text = currentData["nama"].stringValue
        cell.txtAlamat.text = currentData["alamat"].stringValue
        cell.txtTelepon.text = currentData["telepon"].stringValue
        cell.txtBiaya.text = PriceFormatter.format(numPrice: currentData["harga"].numberValue)
        
        let sisa = currentData["sisa"].intValue
        if sisa > 0 {
            cell.viewAvailable.backgroundColor = UIColor(named: .bluePayk)
            cell.txtNumAvailable.text = "Sisa \(sisa)"
        } else {
            cell.viewAvailable.backgroundColor = UIColor(named: .redPayk)
            cell.txtNumAvailable.text = "Penuh"
        }
        
        cell.txtNumAvailable.textColor = UIColor.white
        
        let tipe = currentData["tipe"].stringValue
        cell.txtTipeKos.text = tipe
        
        switch tipe.lowercased() {
        case "putra":
            cell.viewTipe.backgroundColor = UIColor(named: .bluePayk)
        case "putri":
            cell.viewTipe.backgroundColor = UIColor(named: .green)
        default:
            cell.viewTipe.backgroundColor = UIColor(named: .redPayk)
        }
        
        cell.txtTipeKos.textColor = UIColor.white
//        let biaya0 = PriceFormatter.format(numPrice: kamars[0]["biaya"].numberValue)
//        let biaya1 = PriceFormatter.format(numPrice: kamars.last!["biaya"].numberValue)
//        
//        if biaya0 != biaya1 {
//            cell.txtBiaya.text = "\(biaya0) - \(biaya1)"
//        } else {
//            cell.txtBiaya.text = biaya0
//        }
        
        cell.imgKos.af_setImage(withURL: URL(string: currentData["image"].stringValue)!)
        
        return cell
    }
}

extension KosVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = StoryboardScene.Kos.instantiateDetilKosVC()
        vc.data = data[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}
