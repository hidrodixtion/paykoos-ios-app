//
//  DetilKosVC.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit
import SwiftyJSON
import SCLAlertView
import Alamofire

class DetilKosVC: UIViewController {

    @IBOutlet weak var imgKos: UIImageView!
    @IBOutlet weak var txtHarga: UILabel!
    @IBOutlet weak var txtSisaKamar: UILabel!
    @IBOutlet weak var txtTelepon: UILabel!
    @IBOutlet weak var txtAlamat: UILabel!
    @IBOutlet weak var btnPesan: UIButton!
    @IBOutlet weak var txtFitur: UILabel!
    
    var data: JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let data = data else {
            return
        }
        
        title = data["nama"].stringValue
        
        txtHarga.text = "Biaya : \(PriceFormatter.format(numPrice: data["harga"].numberValue)) / bulan"
        
        let sisa = data["sisa"].intValue
        txtSisaKamar.text = "Sisa kamar : \(sisa)"
        
        if sisa <= 0 {
            txtSisaKamar.textColor = UIColor(named: .red)
            btnPesan.isHidden = true
        }
        
        txtTelepon.text = "Telp : \(data["telepon"].stringValue)"
        txtAlamat.text = data["alamat"].stringValue
        let fiturArr = data["fitur"].arrayValue.map { $0.stringValue }
        txtFitur.text = fiturArr.joined(separator: ", ")
        
        imgKos.af_setImage(withURL: URL(string: data["image"].stringValue)!)
        
        tabBarController?.tabBar.isHidden = true
        
        btnPesan.addTarget(self, action: #selector(onBtnPesanClicked), for: .touchUpInside)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        super.viewWillDisappear(animated)
    }
    
    func onBtnPesanClicked() {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alert = SCLAlertView(appearance: appearance)
        
        alert.showWait("Mohon Tunggu", subTitle: "Melakukan proses pemesanan ...", duration: 3.1)
        
        Alamofire.request("http://paykoos.dev/transaksi_new.json").validate().responseJSON { [unowned self] response in
            switch response.result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let value):
                let vc = StoryboardScene.Transaksi.instantiateVcDetilTransaksi()
                vc.data = JSON(rawValue: value)
                
                let when = DispatchTime.now() + 3 // change 2 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }

}
