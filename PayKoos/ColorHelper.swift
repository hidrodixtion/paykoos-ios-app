// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  typealias Color = UIColor
#elseif os(OSX)
  import AppKit.NSColor
  typealias Color = NSColor
#endif

extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
enum ColorName {
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#03a9f4"></span>
  /// Alpha: 100% <br/> (0x03a9f4ff)
  case blue
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#23a5fb"></span>
  /// Alpha: 100% <br/> (0x23a5fbff)
  case bluePayk
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4caf50"></span>
  /// Alpha: 100% <br/> (0x4caf50ff)
  case green
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#fa3d03"></span>
  /// Alpha: 100% <br/> (0xfa3d03ff)
  case orange
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f44336"></span>
  /// Alpha: 100% <br/> (0xf44336ff)
  case red
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f54237"></span>
  /// Alpha: 100% <br/> (0xf54237ff)
  case redPayk
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#fbc12c"></span>
  /// Alpha: 100% <br/> (0xfbc12cff)
  case yelloPayk

  var rgbaValue: UInt32 {
    switch self {
    case .blue:
      return 0x03a9f4ff
    case .bluePayk:
      return 0x23a5fbff
    case .green:
      return 0x4caf50ff
    case .orange:
      return 0xfa3d03ff
    case .red:
      return 0xf44336ff
    case .redPayk:
      return 0xf54237ff
    case .yelloPayk:
      return 0xfbc12cff
    }
  }

  var color: Color {
    return Color(named: self)
  }
}
// swiftlint:enable type_body_length

extension Color {
  convenience init(named name: ColorName) {
    self.init(rgbaValue: name.rgbaValue)
  }
}
