// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation
import UIKit
import PayKoos

// swiftlint:disable file_length
// swiftlint:disable line_length
// swiftlint:disable type_body_length

protocol StoryboardSceneType {
  static var storyboardName: String { get }
}

extension StoryboardSceneType {
  static func storyboard() -> UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }

  static func initialViewController() -> UIViewController {
    guard let vc = storyboard().instantiateInitialViewController() else {
      fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
    }
    return vc
  }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
  func viewController() -> UIViewController {
    return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
  }
  static func viewController(identifier: Self) -> UIViewController {
    return identifier.viewController()
  }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

enum StoryboardScene {
  enum DaftarBank: String, StoryboardSceneType {
    static let storyboardName = "DaftarBank"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case daftarBankVCScene = "DaftarBankVC"
    static func instantiateDaftarBankVC() -> PayKoos.DaftarBankVC {
      guard let vc = StoryboardScene.DaftarBank.daftarBankVCScene.viewController() as? PayKoos.DaftarBankVC
      else {
        fatalError("ViewController 'DaftarBankVC' is not of the expected class PayKoos.DaftarBankVC.")
      }
      return vc
    }
  }
  enum Kos: String, StoryboardSceneType {
    static let storyboardName = "Kos"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case detilKosVCScene = "DetilKosVC"
    static func instantiateDetilKosVC() -> PayKoos.DetilKosVC {
      guard let vc = StoryboardScene.Kos.detilKosVCScene.viewController() as? PayKoos.DetilKosVC
      else {
        fatalError("ViewController 'DetilKosVC' is not of the expected class PayKoos.DetilKosVC.")
      }
      return vc
    }
  }
  enum LaunchScreen: StoryboardSceneType {
    static let storyboardName = "LaunchScreen"
  }
  enum Login: StoryboardSceneType {
    static let storyboardName = "Login"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }
  }
  enum MainPage: String, StoryboardSceneType {
    static let storyboardName = "MainPage"

    case kosVCScene = "KosVC"
    static func instantiateKosVC() -> PayKoos.KosVC {
      guard let vc = StoryboardScene.MainPage.kosVCScene.viewController() as? PayKoos.KosVC
      else {
        fatalError("ViewController 'KosVC' is not of the expected class PayKoos.KosVC.")
      }
      return vc
    }

    case mainPageVCScene = "MainPageVC"
    static func instantiateMainPageVC() -> UITabBarController {
      guard let vc = StoryboardScene.MainPage.mainPageVCScene.viewController() as? UITabBarController
      else {
        fatalError("ViewController 'MainPageVC' is not of the expected class UITabBarController.")
      }
      return vc
    }

    case profileVCScene = "ProfileVC"
    static func instantiateProfileVC() -> PayKoos.ProfileVC {
      guard let vc = StoryboardScene.MainPage.profileVCScene.viewController() as? PayKoos.ProfileVC
      else {
        fatalError("ViewController 'ProfileVC' is not of the expected class PayKoos.ProfileVC.")
      }
      return vc
    }
  }
  enum Transaksi: String, StoryboardSceneType {
    static let storyboardName = "Transaksi"

    case vcDetilTransaksiScene = "VCDetilTransaksi"
    static func instantiateVcDetilTransaksi() -> PayKoos.DetilTransaksiVC {
      guard let vc = StoryboardScene.Transaksi.vcDetilTransaksiScene.viewController() as? PayKoos.DetilTransaksiVC
      else {
        fatalError("ViewController 'VCDetilTransaksi' is not of the expected class PayKoos.DetilTransaksiVC.")
      }
      return vc
    }
  }
}

enum StoryboardSegue {
}

private final class BundleToken {}
