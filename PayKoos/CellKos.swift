//
//  CellKos.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit

class CellKos: UITableViewCell {

    @IBOutlet weak var imgKos: UIImageView!
    @IBOutlet weak var txtKosName: UILabel!
    @IBOutlet weak var txtBiaya: UILabel!
    @IBOutlet weak var txtAlamat: UILabel!
    @IBOutlet weak var txtTelepon: UILabel!
    @IBOutlet weak var txtTipeKos: UILabel!
    @IBOutlet weak var txtNumAvailable: UILabel!
    @IBOutlet weak var viewAvailable: UIView!
    @IBOutlet weak var viewTipe: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
