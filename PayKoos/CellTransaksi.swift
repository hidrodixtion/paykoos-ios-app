//
//  CellTransaksi.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit

class CellTransaksi: UITableViewCell {
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtIDTransaksi: UILabel!
    @IBOutlet weak var txtDateRange: UILabel!
    @IBOutlet weak var txtPaidDate: UILabel!
    @IBOutlet weak var txtCost: UILabel!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var txtInfoPaidDate: UILabel!
    
    var type: TipeTransaksi = .unpaid

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
