//
//  DetilTransaksiVC.swift
//  PayKoos
//
//  Created by Adi Nugroho on 3/11/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit
import SwiftyJSON
import SCLAlertView

enum TipeTransaksi {
    case paid
    case unpaid
}

class DetilTransaksiVC: UIViewController {

    @IBOutlet weak var txtBiaya: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtInfoDate: UILabel!
    @IBOutlet weak var btnBayar: UIButton!
    @IBOutlet weak var btnKonfirmasi: UIButton!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDateRange: UILabel!
    
    var transType: TipeTransaksi = .unpaid
    var data: JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch transType {
        case .paid:
            btnBayar.isHidden = true
            btnKonfirmasi.isHidden = true
            txtInfoDate.text = "Pembayaran diterima pada"
        default:
            break
        }
        
        tabBarController?.tabBar.isHidden = true
        
        guard let data = data else {
            return
        }
        
        let range = DateHelper.parseRange(startDate: data["start_date"].stringValue, endDate: data["end_date"].stringValue)
        txtTitle.text = "Pembayaran untuk \(data["kos"])"
        txtDateRange.text = "Masa tinggal \(range)"
        txtBiaya.text = PriceFormatter.format(numPrice: data["biaya"].numberValue)
        txtDate.text = DateHelper.parse(date: data["expiry_date"].stringValue)
        
        title = data["id"].stringValue
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    @IBAction func onBayarClicked(_ sender: UIButton) {
        let alert = SCLAlertView()
        alert.addButton("BNI Virtual Account") {}
        alert.addButton("Transfer Bank") { [unowned self] in
            self.openTransferBank()
        }
        alert.addButton("Kartu Kredit") {}
        
        alert.showInfo("Metode Pembayaran", subTitle: "* Pembayaran menggunakan transfer bank membutuhkan konfirmasi manual", closeButtonTitle: "Batal")
        
    }
    
    @IBAction func onKonfirmasiClicked(_ sender: UIButton) {
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alert = SCLAlertView(appearance: appearance)
        alert.showWait("Mohon Tunggu", subTitle: "Mengirimkan konfirmasi ...", duration: 2)
    }
    
    func openTransferBank() {
        let vc = StoryboardScene.DaftarBank.instantiateDaftarBankVC()
        navigationController?.pushViewController(vc, animated: true)
    }
}
