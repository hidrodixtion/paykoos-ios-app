# PayKoos iOS App #

PayKoos iOS client project. Built 100% on Swift.

### Setup Project ###

* Install cocoapod
* run `pod install`
* Open **PayKoos.xcworkspace**
* Build
* Run

### Contact ###

* hidrodixtion@gmail.com
* PayKoos Team